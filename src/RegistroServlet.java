
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/registro")
public class RegistroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource dataSource;
	private String titulo = "Registro de Usuarios";
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		try {
			InitialContext context = new InitialContext();
			dataSource = (DataSource) context.lookup("java:comp/env/jdbc/pf");
			if (dataSource == null)
				throw new ServletException("DataSource desconocido");
		} catch (NamingException e) {
	
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session == null) {
			Estado estado;
			String id = request.getParameter("id");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			if (id != null && password != null) { 
				estado = registrarUsuario(request, response, id, email, password);
				if (estado == Estado.OK) {
					RequestDispatcher rd = request.getRequestDispatcher("postregistro");
					rd.forward(request, response);
				}
				else
					HTML.enviarFormUsuario(session, response, "registro", titulo, id, "Registrando usuario...",
							"Registrar usuario", estado,false);
			}
			else
				HTML.enviarFormUsuario(session, response, "registro", titulo, id, "Registrando usuario...",
						"Registrar usuario", Estado.OK,false);
		}
		else
			response.sendRedirect("");
	}

	protected Estado registrarUsuario(HttpServletRequest request, HttpServletResponse response, String id,String email, String password)
			throws IOException {
		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte [] hash = Arrays.copyOf(md.digest((new Date().toString() + email).getBytes()), 33);
			String base64 = Base64.getUrlEncoder().encodeToString(hash);
			StringBuilder cuerpo = new StringBuilder("<h1>Confirma tu eMail</h1>");
			cuerpo.append("<p>Para tener acceso a tu nueva cuenta de usuario necesitas confirmar tu eMail en el enlace siguiente:</p>");
			cuerpo.append(String.format("<p><a href=\"http://localhost/ge/activar?ca=%s\">Confirmar</a>", base64));
			enviarCorreo(email, "Confirmaci�n de eMail", cuerpo.toString());
			
			
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			String sql = String.format("insert into users values ('%s', '%s', '%s', '%s')", id, email,  HashPassword.hashBase64(password), base64);
			statement.executeUpdate(sql);
			return Estado.OK;
		} catch (SQLIntegrityConstraintViolationException e) {
			Logger.getLogger(RegistroServlet.class.getName()).log(Level.SEVERE, null, e);
			return Estado.FALLO_REGISTRO;
		} catch (SQLException | NoSuchAlgorithmException e) {
			Logger.getLogger(RegistroServlet.class.getName()).log(Level.SEVERE, null, e);
			return Estado.ERROR_REGISTRO;
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e1) {
				}
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e1) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
				}
		}
	}
	
	private void enviarCorreo(String to, String asunto, String cuerpo) {
		
		Properties properties = new Properties();

		// Nombre del host de correo (smtp.gmail.com para enviar a través de gmail)
		properties.setProperty("mail.smtp.host", "smtp.gmail.com");

		// TLS si está disponible
		properties.setProperty("mail.smtp.starttls.enable", "true");

		// Puerto de gmail para envio de correos
		properties.setProperty("mail.smtp.port", "587");

		// Si requiere o no usuario y password para conectarse.
		properties.setProperty("mail.smtp.auth", "true");
		
		Session mailSession = Session.getInstance(properties);

		// Para obtener un log de salida más extenso
		mailSession.setDebug(true);
		
		try {
			Message message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress("alumnofpdaw@gmail.com"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(asunto);
			message.setContent(cuerpo, "text/html");
			message.setSentDate(new Date());
			
			Transport.send(message, "alumnofpdaw@gmail.com","practicas2020");		
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

}
